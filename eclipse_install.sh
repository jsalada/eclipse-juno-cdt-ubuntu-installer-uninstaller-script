#!/bin/sh

FILE_URL="http://eclipse.dcc.fc.up.pt/technology/epp/downloads/release/juno/SR1/eclipse-cpp-juno-SR1-linux-gtk-x86_64.tar.gz"
HASH_URL="http://www.eclipse.org/downloads/sums.php?file=/technology/epp/downloads/release/juno/SR1/eclipse-cpp-juno-SR1-linux-gtk-x86_64.tar.gz&type=sha1"
FILE="eclipse-cpp-juno-SR1-linux-gtk-x86_64.tar.gz"

ICON="/usr/share/applications/eclipse.desktop"
INSTALL_PATH="/opt"
BIN_PATH="/usr/bin"
ECLIPSE_FOLDER="eclipse"

OPTS="l"

clear
while [ "$OPTS" != "R" ]&&[ "$OPTS" != "I" ];
do
	echo "Press I to install or R to remove eclipse"
	read OPTS
done

if [ "$OPTS" = "I" ];
then
	if [ ! -f $FILE ];
	then
		echo "Downloading Eclipse..."
        	wget $FILE_URL		
		echo "Downloading hash file to compare against..."
		wget -O eclipse.hash $HASH_URL
	fi

	clear
	echo "Initiating hash file comparison..."
	sha1sum -c eclipse.hash
	if [ $? != 0 ];
	then
		echo "ERROR! Downloaded file hash is not correct. Try to modify FILE_URL and HASH_URL (within this script) to correct targets"
		exit
	fi
	echo "File hash is correct!"
	
	clear
	echo "Proceeding to extraction and installation..."

	tar -xzf $FILE
	if [ -d $INSTALL_PATH/$ECLIPSE_FOLDER ];
	then
		echo "There is already one Eclipse folder at $INSTALL_PATH"
		exit
	fi 
	sudo mv -f eclipse $INSTALL_PATH
	touch tmp
	sudo echo "[Desktop Entry]
		Name=Eclipse 
		Type=Application
		Exec=/opt/eclipse/eclipse
		Terminal=false
		Icon=/opt/eclipse/icon.xpm
		Comment=Integrated Development Environment
		NoDisplay=false
		Categories=Development;IDE
		Name[en]=eclipse.desktop" >> tmp
	sudo mv tmp $ICON
	sudo ln -v -f -t $BIN_PATH -s $INSTALL_PATH/$ECLIPSE_FOLDER/eclipse
	if [ $? != 0 ];
	then
		echo "There was an error!"
	fi
	echo "Eclipse CDT was installed successfully!"

elif [ "$OPTS" = "R" ];
then
	sudo rm $ICON
	sudo rm $BIN_PATH/eclipse
	sudo rm -r $INSTALL_PATH/$ECLIPSE_FOLDER

fi

<<Copyright
Copyright (C) 2013 Joao Salada

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.3
Copyright
